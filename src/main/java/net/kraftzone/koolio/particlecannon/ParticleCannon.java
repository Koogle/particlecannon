package net.kraftzone.koolio.particlecannon;


import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;
import org.bukkit.event.Listener;
import org.bukkit.Material;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import org.bukkit.entity.Player;
import java.util.HashMap;
import org.bukkit.plugin.java.JavaPlugin;


public class ParticleCannon extends JavaPlugin implements Listener
{
    private PlayerListener listener;
    public HashMap<Player, Long> cooldown;
    public WorldGuardPlugin worldGuard;
    public HashMap<Material, Boolean> block_materials;
    public boolean destroyNotListed = true;
    public boolean destroyAll = false;

    public ParticleCannon() {
        this.cooldown = new HashMap<Player, Long>();
        this.block_materials = new HashMap<Material, Boolean>();
    }

    public void onEnable() {

        this.worldGuard = WorldGuardPlugin.inst();
        this.getConfig().options().copyDefaults(true);
        this.saveConfig();
        destroyNotListed = this.getConfig().getBoolean("Destroy-Not-Listed-Blocks");
        destroyAll = this.getConfig().getBoolean("Destroy-All");
        Material[] values;
        for (int length = (values = Material.values()).length, i = 0; i < length; ++i) {
            final Material m = values[i];
            if (m.isBlock()) {
                try {
                    if(this.getConfig().contains("Destroy-" + m.name())){
                        this.block_materials.put(m, this.getConfig().getBoolean("Destroy-" + m.name()));
                    }
                }
                catch (Exception e) {
                    this.getServer().getConsoleSender().sendMessage("§cDestroy-" + m.name() + " Config not defined!!");
                }
            }
        }
        this.saveConfig();
        this.listener = new PlayerListener(this);
        this.getServer().getPluginManager().registerEvents((Listener)this.listener, (Plugin)this);
    }

    public WorldGuardPlugin getWorldGuard() {
        final Plugin plugin = this.getServer().getPluginManager().getPlugin("WorldGuard");
        if (plugin == null || !(plugin instanceof WorldGuardPlugin)) {
            return null;
        }
        return (WorldGuardPlugin)plugin;
    }

    public boolean onCommand(final CommandSender sender, final Command command, final String label, final String[] args) {
        if (!sender.hasPermission("particlecannons.admin") && !sender.isOp()) {
            return false;
        }
        if (command.getName().equalsIgnoreCase("PCReload")) {
            this.reloadConfig();
            sender.sendMessage("[ParticleCannons] Config Reloaded");
            return true;
        }

        if (command.getName().equalsIgnoreCase("PCSetRange")) {
            try {
                if(args.length == 0 ) {
                    sender.sendMessage("[ParticleCannons] Cannon-Item is currently: §a"+this.getConfig().get("Cannon-Item").toString());
                    return true;
                }
                this.getConfig().set("Range", (Object)Integer.parseInt(args[0]));
                this.saveConfig();
                sender.sendMessage("[ParticleCannons] Cannon range set to §a" + args[0]);
            }
            catch (Exception e) {
                sender.sendMessage("§cIncorrect usage, use §a/PCSetRange <range>");
            }
        }
        else if (command.getName().equalsIgnoreCase("PCSetItems")) {
            try {
                //this.getConfig().set("Cannon-Item", (Object)Integer.parseInt(args[0]));
                if(Material.getMaterial(args[0].toUpperCase()) != null){
                    this.getConfig().set("Cannon-Item", args[0]);
                }else{

                    sender.sendMessage("[ParticleCannons] §a" + args[0] + " is not a valid material");
                }
                //this.getConfig().set("Ammo-Item", (Object)Integer.parseInt(args[1]));
                if(Material.getMaterial(args[1].toUpperCase()) != null){
                    this.getConfig().set("Ammo-Item", args[1]);
                }else{
                    sender.sendMessage("[ParticleCannons] §a" + args[1] + " is not a valid material");
                }
                this.getConfig().set("Ammo-per-Shot", (Object)Integer.parseInt(args[2]));
                this.saveConfig();
                sender.sendMessage("[ParticleCannons] Items set to §a" + args[0] + ", " + args[1] + ", " + args[2]);
            }
            catch (Exception e) {
                sender.sendMessage("§cIncorrect usage, use §a/PCSetRange <Cannon item ID> <Ammo item ID> <Ammo used per shot");
            }
        }
        else if (command.getName().equalsIgnoreCase("PCToggleGrief")) {
            try {
                this.getConfig().set("Explosion-Grief", (Object)!this.getConfig().getBoolean("Explosion-Grief"));
                this.saveConfig();
                sender.sendMessage("[ParticleCannons] Explosion grief turned to §a" + this.getConfig().getString("Explosion-Grief"));
            }
            catch (Exception e) {
                sender.sendMessage("§cIncorrect usage, use §a/PCToggleGrief");
            }
        }
        return true;
    }
}
