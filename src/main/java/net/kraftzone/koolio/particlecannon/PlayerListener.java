package net.kraftzone.koolio.particlecannon;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
//import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldguard.protection.flags.Flags;
import org.bukkit.entity.FallingBlock;
import org.bukkit.block.Block;
import org.bukkit.util.Vector;
import org.bukkit.entity.LivingEntity;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;
import org.bukkit.entity.Player;
import java.util.Map;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.Material;
import java.util.ArrayList;
import java.util.Random;
import org.bukkit.event.Listener;

public class PlayerListener implements Listener
{
    private ParticleCannon plugin;
    private Random random;
    private ArrayList<Material> disallowedBlocks;

    public PlayerListener(final ParticleCannon plugin) {
        this.random = new Random();
        (this.disallowedBlocks = new ArrayList<Material>()).clear();
        //this.disallowedBlocks.add(Material.PISTON_BASE);
        this.disallowedBlocks.add(Material.PISTON);
        //this.disallowedBlocks.add(Material.PISTON_EXTENSION);
        this.disallowedBlocks.add(Material.PISTON_HEAD);
        //this.disallowedBlocks.add(Material.PISTON_MOVING_PIECE);
        this.disallowedBlocks.add(Material.MOVING_PISTON);
        //this.disallowedBlocks.add(Material.PISTON_STICKY_BASE);
        this.disallowedBlocks.add(Material.STICKY_PISTON);
        this.disallowedBlocks.add(Material.SNOW);
        this.plugin = plugin;
    }

    @EventHandler
    public void onClick(final PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            final Player p = event.getPlayer();
            //p.sendMessage("Shooting right click");
            if (p.getInventory().getItemInMainHand().getType() == Material.getMaterial(this.plugin.getConfig().getString("Cannon-Item"))) {
//                if(p.isOp()){
//                    this.fireGun(event.getPlayer());
//                    return;
//                }
                if (p.hasPermission("particlecannons.use")) {
                    final long cooldown = this.plugin.getConfig().getLong("Cooldown");
                    for (final Map.Entry<Player, Long> entry : this.plugin.cooldown.entrySet()) {
                        if (System.currentTimeMillis() - entry.getValue() > cooldown) {
                            this.plugin.cooldown.remove(entry.getKey());
                        }
                    }
                    if (!this.plugin.cooldown.containsKey(p)) {
                        Material AmmoItem = Material.getMaterial(this.plugin.getConfig().getString("Ammo-Item"));
                        if (p.getInventory().containsAtLeast(new ItemStack(AmmoItem), this.plugin.getConfig().getInt("Ammo-per-Shot"))) {
                            this.plugin.cooldown.put(p, System.currentTimeMillis());
                            this.fireGun(event.getPlayer());
                            p.getInventory().removeItem(new ItemStack[] { new ItemStack(AmmoItem, this.plugin.getConfig().getInt("Ammo-per-Shot")) });
                            p.updateInventory();
                        }
                        else {
                            p.sendMessage(this.plugin.getConfig().getString("No-Ammo-Message"));
                        }
                    }
                    else {
                        final double cd = (int)((this.plugin.getConfig().getLong("Cooldown") - (System.currentTimeMillis() - this.plugin.cooldown.get(p))) / 100L);
                        p.sendMessage(this.plugin.getConfig().getString("Cool-Down-Message").replace("[time]", Double.toString(cd / 10.0)));
                    }
                }
                else {
                    p.sendMessage(this.plugin.getConfig().getString("No-Permission-Message"));
                }
            }
        }
    }

    public void fireGun(final Player player) {
        final int dropChance = this.plugin.getConfig().getInt("Block-Drop-Chance");
        final int damage = this.plugin.getConfig().getInt("Entity-Damage");
        final int particle = this.plugin.getConfig().getInt("Particle-Type") % 5 + 12;
        final int particleMod = this.plugin.getConfig().getInt("Particle-Modifier");
        final double blockDamage = this.plugin.getConfig().getDouble("Block-Damage");
        final double selfKnockBack = this.plugin.getConfig().getDouble("Knockback-Self");
        final double otherKnockBack = this.plugin.getConfig().getDouble("Knockback-Others");
        final double radius = this.plugin.getConfig().getDouble("Blast-Radius");
        final double radiusSquared = radius * radius;
        final boolean grief = this.plugin.getConfig().getBoolean("Explosion-Grief");
        if (this.plugin.getConfig().getBoolean("Super-Secret-Setting")) {
            this.fireSecret(player, radius, radiusSquared, 20);
            return;
        }
        final Location pl = player.getEyeLocation();
        final double px = pl.getX();
        final double py = pl.getY();
        final double pz = pl.getZ();
        final double yaw = Math.toRadians(pl.getYaw() + 90.0f);
        final double pitch = Math.toRadians(pl.getPitch() + 90.0f);
        final double x = Math.sin(pitch) * Math.cos(yaw);
        final double y = Math.cos(pitch);
        final double z = Math.sin(pitch) * Math.sin(yaw);
        player.damage(this.plugin.getConfig().getInt("Self-Damage"));
        Location loc = new Location(player.getWorld(), px + 1.9 * x, py + 1.9 * y, pz + 1.9 * z);
        player.getWorld().createExplosion(loc, 0.0f);
        this.createExplosion(loc, player, radius, radiusSquared, blockDamage, selfKnockBack, otherKnockBack, damage, dropChance, grief);
        for (int i = 2; i <= this.plugin.getConfig().getInt("Range"); ++i) {
            loc = new Location(player.getWorld(), px + i * x, py + i * y, pz + i * z);
            if (loc.getBlock().getType() != Material.AIR) {
                break;
            }
            player.getWorld().createExplosion(loc, 0.0f);
            this.createExplosion(loc, player, radius, radiusSquared, blockDamage, selfKnockBack, otherKnockBack, damage, dropChance, grief);
            player.getWorld().playEffect(loc, Effect.values()[particle], particleMod);
        }
    }

    public void createExplosion(final Location loc, final Player player, final double radius, final double radiusSquared, final double blockDamage, final double selfKnockBack, final double otherKnockBack, final int damage, final int dropChance, final boolean grief) {
        for (final LivingEntity e : loc.getWorld().getLivingEntities()) {
            if (e.getEyeLocation().distanceSquared(loc) <= radiusSquared) {
                if (e != player) {
                    if (!this.canHurt(player, e)) {
                        continue;
                    }
                    e.damage(damage);
                    final double vx = e.getEyeLocation().getX() - loc.getX();
                    final double vy = e.getEyeLocation().getY() - loc.getY();
                    final double vz = e.getEyeLocation().getZ() - loc.getZ();
                    e.setVelocity(e.getVelocity().add(new Vector(vx, vy, vz).multiply(otherKnockBack / ((e.getEyeLocation().distanceSquared(loc) < 1.0) ? 1.0 : e.getEyeLocation().distanceSquared(loc)))));
                    e.setFallDistance(0.0f);
                }
                else {
                    e.damage(0);
                    final double vx = e.getEyeLocation().getX() - loc.getX();
                    final double vy = e.getEyeLocation().getY() - loc.getY();
                    final double vz = e.getEyeLocation().getZ() - loc.getZ();
                    e.setVelocity(e.getVelocity().add(new Vector(vx, vy, vz).multiply(selfKnockBack / ((e.getEyeLocation().distanceSquared(loc) < 1.0) ? 1.0 : e.getEyeLocation().distanceSquared(loc)))));
                    e.setFallDistance(0.0f);
                }
            }
        }
        if (grief) {
            final boolean createBlock = false;
            final Material createMaterial = Material.GLASS;
            final int r = (int)Math.ceil(radius);
            final int xx = (int)Math.round(loc.getX());
            final int yy = (int)Math.round(loc.getY());
            final int zz = (int)Math.round(loc.getZ());
            for (int x = 0 - r; x <= r; ++x) {
                for (int y = 0 - r; y <= r; ++y) {
                    for (int z = 0 - r; z <= r; ++z) {
                        final Block block = player.getWorld().getBlockAt(x + xx, y + yy, z + zz);
                            //if (this.plugin.block_materials.containsKey(block.getType()) && block.getLocation().distanceSquared(loc) <= radiusSquared && this.canBuild(player, block)) {

                            if (checkBlock(player, block.getType()) && block.getLocation().distanceSquared(loc) <= radiusSquared && this.canBuild(player, block)) {
                                if(createBlock){
                                    block.setType(createMaterial);
                                }else {
                                    if (dropChance >= 1 && this.random.nextInt(dropChance) == 0) {
                                        block.breakNaturally(new ItemStack(Material.DIAMOND_AXE));
                                    } else {
                                        block.setType(Material.AIR);
                                    }
                                }
                            }else{
                                //player.sendMessage("   if (block.getLocation().distanceSquared(loc) <= radiusSquared && this.canBuild(player, block))");
                            }

                    }
                }
            }
        }
    }

    public boolean checkBlock(Player p, Material block){
        if(this.plugin.destroyAll && p.isOp()){
            return true;
        }
        if(this.plugin.block_materials.containsKey(block)){
            return this.plugin.block_materials.get(block);
        }
        return this.plugin.destroyNotListed;
    }

    public void fireSecret(final Player player, final double radius, final double radiusSquared, final int range) {
        final ArrayList<Block> blocks = new ArrayList<Block>();
        final Location pl = player.getEyeLocation();
        final double px = pl.getX();
        final double py = pl.getY();
        final double pz = pl.getZ();
        final double yaw = Math.toRadians(pl.getYaw() + 90.0f);
        final double pitch = Math.toRadians(pl.getPitch() + 90.0f);
        double x = Math.sin(pitch) * Math.cos(yaw);
        double y = Math.cos(pitch);
        double z = Math.sin(pitch) * Math.sin(yaw);
        Location loc = new Location(player.getWorld(), px + 1.9 * x, py + 1.9 * y, pz + 1.9 * z);
        player.getWorld().createExplosion(loc, 0.0f);
        for (int i = 2; i <= range; ++i) {
            loc = new Location(player.getWorld(), px + i * x, py + i * y, pz + i * z);
            player.getWorld().createExplosion(loc, 0.0f);
            final int r = (int)Math.ceil(radius);
            final int xx = (int)Math.round(loc.getX());
            final int yy = (int)Math.round(loc.getY());
            final int zz = (int)Math.round(loc.getZ());
            for (int lx = 0 - r; lx <= r; ++lx) {
                for (int ly = 0 - r; ly <= r; ++ly) {
                    for (int lz = 0 - r; lz <= r; ++lz) {
                        final Block block = player.getWorld().getBlockAt(lx + xx, ly + yy, lz + zz);
                        if (this.canBuild(player, block) && block.getLocation().distanceSquared(loc) <= radiusSquared && block.getType().isSolid() && !blocks.contains(block) && !this.disallowedBlocks.contains(block.getType()) && this.plugin.block_materials.get(block.getType())) {
                            blocks.add(block);
                        }
                    }
                }
            }
        }
        final ArrayList<FallingBlock> fblocks = new ArrayList<FallingBlock>();
        for (int j = 0; j < blocks.size(); ++j) {
            final Block b = blocks.get(j);
            final Location bLoc = b.getLocation();
            final FallingBlock fb = player.getWorld().spawnFallingBlock(bLoc, b.getType(), b.getData());
            fblocks.add(fb);
            b.setType(Material.AIR);
        }
        for (int j = 0; j < fblocks.size(); ++j) {
            final Block b = blocks.get(j);
            final FallingBlock fb2 = fblocks.get(j);
            final Location bLoc2 = b.getLocation();
            x = bLoc2.getX() - player.getEyeLocation().getX() + 0.5;
            y = bLoc2.getY() - player.getEyeLocation().getY() + 0.5;
            z = bLoc2.getZ() - player.getEyeLocation().getZ() + 0.5;
            fb2.setDropItem(false);
            fb2.setVelocity(new Vector(x, y, z).multiply(10.0 / player.getEyeLocation().distance(loc)));
        }
    }

    public boolean canBuild(final Player player, final Block b) {
        //player.sendMessage("canBuild " + b.toString());
        if (this.plugin.worldGuard == null) {
            return true;
        }
        final RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(new BukkitWorld(b.getWorld()));
        if (regionManager != null) {
            final ApplicableRegionSet set = regionManager.getApplicableRegions(BukkitAdapter.asBlockVector(b.getLocation()));
            if (!set.testState(WorldGuardPlugin.inst().wrapPlayer(player), Flags.OTHER_EXPLOSION)) {
                return false;
            }
            if(set.testState(WorldGuardPlugin.inst().wrapPlayer(player), Flags.BUILD)){
                return true;
            }
        }
//        return this.plugin.worldGuard.canBuild(player, b);
        return true;
    }

    public boolean canHurt(final Player player, final LivingEntity e) {
        if (this.plugin.worldGuard == null) {
            return true;
        }
        //final RegionManager regionManager = this.plugin.worldGuard.getRegionManager(e.getWorld());
        final RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(new BukkitWorld(e.getWorld()));
        if (regionManager != null) {
            final ApplicableRegionSet set = regionManager.getApplicableRegions(BukkitAdapter.asBlockVector(e.getLocation()));

            //if (!set.allows(Flags.MOB_DAMAGE) && !(e instanceof Player)) {
            if (!set.testState(WorldGuardPlugin.inst().wrapPlayer(player), Flags.MOB_DAMAGE) && !(e instanceof Player)) {
                return false;
            }
            //if (!set.allows(Flags.PVP) && e instanceof Player) {
            if (!set.testState(WorldGuardPlugin.inst().wrapPlayer(player), Flags.PVP) && e instanceof Player) {
                return false;
            }
        }
        return true;
    }
}
